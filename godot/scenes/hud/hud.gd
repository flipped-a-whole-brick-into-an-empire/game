extends Control



# Called when the node enters the scene tree for the first time.
func _ready():
	pass

var sum = 0

var heart = preload("res://scenes/hud/srdicko.tscn")

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var poggers = $MarginContainer/ProgressBar.value - delta
	if poggers < 0:
		$MarginContainer/ProgressBar.value = 0
	else:
		$MarginContainer/ProgressBar.value -= delta

func add_heart(count : int = 1):
	for i in range(count):
		var h1 = heart.instantiate()
		$MarginContainer2/GridContainer.add_child(h1)

func remove_heart(count : int = 1):
	for i in range(count):
		if $MarginContainer2/GridContainer.get_child_count() > i:
			var h2 = $MarginContainer2/GridContainer.get_child(i)
			h2.queue_free()

func attack(cooldown : float):
	$MarginContainer/ProgressBar.max_value = cooldown
	$MarginContainer/ProgressBar.value = cooldown
