extends Player

var m_delta : Vector2

@export var sensitivity : float

func _input(event):
	if get_tree().paused == true: return
	if event is InputEventMouseMotion:
		#print_debug("movement")
		m_delta += event.relative
	elif event is InputEventMouseButton:
		Input.mouse_mode = Input.MOUSE_MODE_CAPTURED
		
func _process(delta):
	#print_debug("aaa")
	rotation_degrees.x += -(m_delta).y * sensitivity
	if rotation_degrees.x > 90: rotation_degrees.x = 90
	elif rotation_degrees.x < -90: rotation_degrees.x = -90
	rotation_degrees.y += -(m_delta).x * sensitivity
	m_delta = Vector2.ZERO
	
	if Input.is_action_just_pressed("ui_cancel"):
		var pause = preload("res://scenes/menu/pause_menu.tscn").instantiate()
		add_sibling(pause)
		get_tree().paused = true
