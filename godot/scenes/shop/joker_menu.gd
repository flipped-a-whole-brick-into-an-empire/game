extends Control

signal JokerChoice(pick : int)

func _ready():
	$AnimationPlayer.play("fader")

func _on_button_pressed():
	$MarginContainer/PanelContainer/MarginContainer/VBoxContainer/HBoxContainer/Button.disabled = true
	$MarginContainer/PanelContainer/MarginContainer/VBoxContainer/HBoxContainer/Button2.disabled = true
	$MarginContainer/PanelContainer/MarginContainer/VBoxContainer/HBoxContainer/Button3.disabled = true
	$MarginContainer/PanelContainer/MarginContainer/VBoxContainer/HBoxContainer/Button4.disabled = true
	JokerChoice.emit(0)
	$AnimationPlayer.play_backwards("fader")
	await get_tree().create_timer(1).timeout
	queue_free()

func _on_button_2_pressed():
	$MarginContainer/PanelContainer/MarginContainer/VBoxContainer/HBoxContainer/Button.disabled = true
	$MarginContainer/PanelContainer/MarginContainer/VBoxContainer/HBoxContainer/Button2.disabled = true
	$MarginContainer/PanelContainer/MarginContainer/VBoxContainer/HBoxContainer/Button3.disabled = true
	$MarginContainer/PanelContainer/MarginContainer/VBoxContainer/HBoxContainer/Button4.disabled = true
	JokerChoice.emit(1)
	$AnimationPlayer.play_backwards("fader")
	await get_tree().create_timer(1).timeout
	queue_free()

func _on_button_3_pressed():
	$MarginContainer/PanelContainer/MarginContainer/VBoxContainer/HBoxContainer/Button.disabled = true
	$MarginContainer/PanelContainer/MarginContainer/VBoxContainer/HBoxContainer/Button2.disabled = true
	$MarginContainer/PanelContainer/MarginContainer/VBoxContainer/HBoxContainer/Button3.disabled = true
	$MarginContainer/PanelContainer/MarginContainer/VBoxContainer/HBoxContainer/Button4.disabled = true
	JokerChoice.emit(2)
	$AnimationPlayer.play_backwards("fader")
	await get_tree().create_timer(1).timeout
	queue_free()

func _on_button_4_pressed():
	$MarginContainer/PanelContainer/MarginContainer/VBoxContainer/HBoxContainer/Button.disabled = true
	$MarginContainer/PanelContainer/MarginContainer/VBoxContainer/HBoxContainer/Button2.disabled = true
	$MarginContainer/PanelContainer/MarginContainer/VBoxContainer/HBoxContainer/Button3.disabled = true
	$MarginContainer/PanelContainer/MarginContainer/VBoxContainer/HBoxContainer/Button4.disabled = true
	JokerChoice.emit(3)
	$AnimationPlayer.play_backwards("fader")
	await get_tree().create_timer(1).timeout
	queue_free()
