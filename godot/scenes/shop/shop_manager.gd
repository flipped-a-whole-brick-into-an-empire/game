extends Node2D

var c11 = preload("res://assets/shop/cards/choice11.stylebox")
var c12 = preload("res://assets/shop/cards/choice12.stylebox")
var c13 = preload("res://assets/shop/cards/choice13.stylebox")

var c21 = preload("res://assets/shop/cards/choice21.stylebox")
var c22 = preload("res://assets/shop/cards/choice22.stylebox")
var c23 = preload("res://assets/shop/cards/choice23.stylebox")

var c31 = preload("res://assets/shop/cards/choice31.stylebox")
var c32 = preload("res://assets/shop/cards/choice32.stylebox")
var c33 = preload("res://assets/shop/cards/choice33.stylebox")

var choice_on = preload("res://assets/shop/choice_box1.stylebox")
var choice_off = preload("res://assets/shop/choice_box3.stylebox")

#@onready var b1 : Button = $MarginContainer/MainSplit/ShopSection/Choices/Choice1/ChoiceSplitter/MarginContainer/Panel/CardButton
#@onready var b2 : Button = $MarginContainer/MainSplit/ShopSection/Choices/Choice2/ChoiceSplitter/MarginContainer/Panel/CardButton
#@onready var b3 : Button = $MarginContainer/MainSplit/ShopSection/Choices/Choice3/ChoiceSplitter/MarginContainer/Panel/CardButton

var _cards = {
	0 : 	{"texture": "res://assets/shop/cards/clubs/clubs_3.png",		"attack": 0, "defense": 0.015, "health": 0, "speed": 0, "joker": false, "price": 7},
	1 : 	{"texture": "res://assets/shop/cards/clubs/clubs_7.png",		"attack": 0, "defense": 0.035, "health": 0, "speed": 0, "joker": false, "price": 15},
	2 : 	{"texture": "res://assets/shop/cards/clubs/clubs_J.png",		"attack": 0, "defense": 0.065, "health": 0, "speed": 0, "joker": false, "price": 26},
	3 : 	{"texture": "res://assets/shop/cards/spades/spades_2.png",		"attack": 2, "defense": 0, "health": 0, "speed": 0, 	"joker": false, "price": 6},
	4 : 	{"texture": "res://assets/shop/cards/spades/spades_4.png",		"attack": 4, "defense": 0, "health": 0, "speed": 0, 	"joker": false, "price": 11},
	5 : 	{"texture": "res://assets/shop/cards/spades/spades_9.png",		"attack": 9, "defense": 0, "health": 0, "speed": 0, 	"joker": false, "price": 22},
	6 : 	{"texture": "res://assets/shop/cards/hearts/hearts_5.png",		"attack": 0, "defense": 0, "health": 1, "speed": 0, 	"joker": false, "price": 9},
	7 : 	{"texture": "res://assets/shop/cards/hearts/hearts_10.png",		"attack": 0, "defense": 0, "health": 2, "speed": 0, 	"joker": false, "price": 16},
	8 : 	{"texture": "res://assets/shop/cards/hearts/hearts_K.png",		"attack": 0, "defense": 0, "health": 3, "speed": 0, 	"joker": false, "price": 23},
	9 : 	{"texture": "res://assets/shop/cards/diamonds/diamonds_4.png",	"attack": 0, "defense": 0, "health": 0, "speed": 4, 	"joker": false, "price": 5},
	10 : 	{"texture": "res://assets/shop/cards/diamonds/diamonds_8.png",	"attack": 0, "defense": 0, "health": 0, "speed": 8, 	"joker": false, "price": 9},
	11 : 	{"texture": "res://assets/shop/cards/diamonds/diamonds_Q.png",	"attack": 0, "defense": 0, "health": 0, "speed": 12, 	"joker": false, "price": 13},
	12 :	{"texture": "res://assets/shop/cards/joker.png",				"attack": 0, "defense": 0, "health": 0, "speed": 0, 	"joker": true,	"price": 20}
}

var card1
var card2
var card3

var c1bought = false
var c2bought = false
var c3bought = false

var reroll_cost : int = 3

func _ready():
	#c11.texture = load("res://assets/shop/cards/hearts/hearts_10.png")
	#c12.texture = load("res://assets/shop/cards/hearts/hearts_10.png")
	#c13.texture = load("res://assets/shop/cards/hearts/hearts_10.png")
	
	_randomize()
	
	$FadeModuleBlack.play("FadeIn")
	
	await get_tree().create_timer(1).timeout
	$MarginContainer/MainSplit/ShopSection/Choices/Choice1/AnimationPlayer.play("fliṕ")
	await get_tree().create_timer(0.1).timeout
	$MarginContainer/MainSplit/ShopSection/Choices/Choice2/AnimationPlayer.play("fliṕ")
	await get_tree().create_timer(0.1).timeout
	$MarginContainer/MainSplit/ShopSection/Choices/Choice3/AnimationPlayer.play("fliṕ")
	

func _process(delta):
	var s = $"/root/PlayerData".souls
	$MarginContainer/MainSplit/StatsSection/SoulsPanel/SoulsWrapper/SoulsText.text = "souls: %s" % s
	
	if card1["price"] > s or c1bought:
		$MarginContainer/MainSplit/ShopSection/Choices/Choice1/ChoiceSplitter/MarginContainer/Panel/CardButton.disabled = true
	else:
		$MarginContainer/MainSplit/ShopSection/Choices/Choice1/ChoiceSplitter/MarginContainer/Panel/CardButton.disabled = false
	if card2["price"] > s or c2bought:
		$MarginContainer/MainSplit/ShopSection/Choices/Choice2/ChoiceSplitter/MarginContainer/Panel/CardButton.disabled = true
	else: 
		$MarginContainer/MainSplit/ShopSection/Choices/Choice2/ChoiceSplitter/MarginContainer/Panel/CardButton.disabled = false
	if card3["price"] > s or c3bought: 
		$MarginContainer/MainSplit/ShopSection/Choices/Choice3/ChoiceSplitter/MarginContainer/Panel/CardButton.disabled = true
	else: 
		$MarginContainer/MainSplit/ShopSection/Choices/Choice3/ChoiceSplitter/MarginContainer/Panel/CardButton.disabled = false
	if reroll_cost > s: 
		$MarginContainer/MainSplit/ShopSection/RerollButton.disabled = true
	else: 
		$MarginContainer/MainSplit/ShopSection/RerollButton.disabled = false
	
	if not c1bought: $MarginContainer/MainSplit/ShopSection/Choices/Choice1.add_theme_stylebox_override("panel", choice_on)
	if not c2bought: $MarginContainer/MainSplit/ShopSection/Choices/Choice2.add_theme_stylebox_override("panel", choice_on)
	if not c3bought: $MarginContainer/MainSplit/ShopSection/Choices/Choice3.add_theme_stylebox_override("panel", choice_on)
		
	
	var a = $"/root/PlayerData".attack_speed
	var d = $"/root/PlayerData".dodge_chance
	var h = $"/root/PlayerData".max_health
	var m = $"/root/PlayerData".movement_speed
	
	$MarginContainer/MainSplit/StatsSection/StatsPanel/StatsWrapper/StatsText.text = "stats:\n   attack: %s\n   defense: %s\n   health: %s\n   move: %s" % [ a, d, h, m ]

func _randomize():
	card1 = _cards[randi() % _cards.size()]
	card2 = _cards[randi() % _cards.size()]
	while card2 == card1: card2 = _cards[randi() % _cards.size()]
	card3 = _cards[randi() % _cards.size()]
	while card3 == card2 or card3 == card1: card3 = _cards[randi() % _cards.size()]
	
	_set_b1(card1["texture"])
	_set_b2(card2["texture"])
	_set_b3(card3["texture"])
	
	$MarginContainer/MainSplit/ShopSection/Choices/Choice1/ChoiceSplitter/MarginContainer2/PriceLabel.text = "souls: %s" % card1["price"]
	$MarginContainer/MainSplit/ShopSection/Choices/Choice2/ChoiceSplitter/MarginContainer2/PriceLabel.text = "souls: %s" % card2["price"]
	$MarginContainer/MainSplit/ShopSection/Choices/Choice3/ChoiceSplitter/MarginContainer2/PriceLabel.text = "souls: %s" % card3["price"]

func _reroll():
	$"/root/PlayerData".souls -= reroll_cost
	$MarginContainer/MainSplit/ShopSection/RerollButton.disabled = true
	$MarginContainer/MainSplit/ShopSection/RerollButton.text = "REROLLING"
	
	$MarginContainer/MainSplit/ShopSection/Choices/Choice1/AnimationPlayer.play_backwards("fliṕ")
	await get_tree().create_timer(0.1).timeout
	$MarginContainer/MainSplit/ShopSection/Choices/Choice2/AnimationPlayer.play_backwards("fliṕ")
	await get_tree().create_timer(0.1).timeout
	$MarginContainer/MainSplit/ShopSection/Choices/Choice3/AnimationPlayer.play_backwards("fliṕ")
	await get_tree().create_timer(0.65).timeout
	
	_randomize()
	
	c1bought = false
	c2bought = false
	c3bought = false
	
	$MarginContainer/MainSplit/ShopSection/Choices/Choice1/AnimationPlayer.play("fliṕ")
	await get_tree().create_timer(0.1).timeout
	$MarginContainer/MainSplit/ShopSection/Choices/Choice2/AnimationPlayer.play("fliṕ")
	await get_tree().create_timer(0.1).timeout
	$MarginContainer/MainSplit/ShopSection/Choices/Choice3/AnimationPlayer.play("fliṕ")
	await get_tree().create_timer(0.15).timeout
	
	reroll_cost = ceil(reroll_cost*1.5)
	
	$MarginContainer/MainSplit/ShopSection/RerollButton.disabled = false
	$MarginContainer/MainSplit/ShopSection/RerollButton.text = "REROLL - %s souls" % reroll_cost
	
func _set_b1(tex : String):
	c11.texture = load(tex)
	c12.texture = load(tex)
	c13.texture = load(tex)
func _set_b2(tex : String):
	c21.texture = load(tex)
	c22.texture = load(tex)
	c23.texture = load(tex)
func _set_b3(tex : String):
	c31.texture = load(tex)
	c32.texture = load(tex)
	c33.texture = load(tex)
	

func _on_reroll_button_pressed():
	_reroll()

func _on_card1_button_pressed():
	$MarginContainer/MainSplit/ShopSection/Choices/Choice1/ChoiceSplitter/MarginContainer/Panel/CardButton.disabled = true
	c1bought = true
	$MarginContainer/MainSplit/ShopSection/Choices/Choice1.add_theme_stylebox_override("panel", choice_off)
	$MarginContainer/MainSplit/ShopSection/Choices/Choice1/ChoiceSplitter/MarginContainer2/PriceLabel.text = "sould"
	$"/root/PlayerData".souls -= card1["price"]
	
	if card1["joker"]:
		var joke = preload("res://scenes/shop/joker_menu.tscn").instantiate()
		joke.connect("JokerChoice", _joker_stat_assignment)
		add_sibling(joke)
		return
	
	$"/root/PlayerData".attack_speed -= card1["attack"]*($"/root/PlayerData".attack_speed)/50
	$"/root/PlayerData".dodge_chance += card1["defense"]
	$"/root/PlayerData".max_health += card1["health"]
	$"/root/PlayerData".movement_speed += card1["speed"]/2
	

func _on_card2_button_pressed():
	$MarginContainer/MainSplit/ShopSection/Choices/Choice2/ChoiceSplitter/MarginContainer/Panel/CardButton.disabled = true
	c2bought = true
	$MarginContainer/MainSplit/ShopSection/Choices/Choice2.add_theme_stylebox_override("panel", choice_off)
	$MarginContainer/MainSplit/ShopSection/Choices/Choice2/ChoiceSplitter/MarginContainer2/PriceLabel.text = "sould"
	$"/root/PlayerData".souls -= card2["price"]
	
	if card2["joker"]:
		var joke = preload("res://scenes/shop/joker_menu.tscn").instantiate()
		joke.connect("JokerChoice", _joker_stat_assignment)
		add_sibling(joke)
		return
	
	$"/root/PlayerData".attack_speed -= card2["attack"]*($"/root/PlayerData".attack_speed)/50
	$"/root/PlayerData".dodge_chance += card2["defense"]
	$"/root/PlayerData".max_health += card2["health"]
	$"/root/PlayerData".movement_speed += card2["speed"]/2
	
	

func _on_card3_button_pressed():
	$MarginContainer/MainSplit/ShopSection/Choices/Choice3/ChoiceSplitter/MarginContainer/Panel/CardButton.disabled = true
	c3bought = true
	$MarginContainer/MainSplit/ShopSection/Choices/Choice3.add_theme_stylebox_override("panel", choice_off)
	$MarginContainer/MainSplit/ShopSection/Choices/Choice3/ChoiceSplitter/MarginContainer2/PriceLabel.text = "sould"
	$"/root/PlayerData".souls -= card3["price"]
	
	if card3["joker"]:
		var joke = preload("res://scenes/shop/joker_menu.tscn").instantiate()
		joke.connect("JokerChoice", _joker_stat_assignment)
		add_sibling(joke)
		return
	
	$"/root/PlayerData".attack_speed -= card3["attack"]*($"/root/PlayerData".attack_speed)/50
	$"/root/PlayerData".dodge_chance += card3["defense"]
	$"/root/PlayerData".max_health += card3["health"]
	$"/root/PlayerData".movement_speed += card3["speed"]/3
	
func _joker_stat_assignment(c):
	if c == 0:
		$"/root/PlayerData".attack_speed -= 5*($"/root/PlayerData".attack_speed)/50
	elif c == 1:
		$"/root/PlayerData".dodge_chance += 0.04
	elif c == 2:
		$"/root/PlayerData".max_health += 2
	elif c == 3:
		$"/root/PlayerData".movement_speed += 4

func _on_summon_button_pressed():
	get_tree().change_scene_to_file("res://scenes/tard_wrangler.tscn")
