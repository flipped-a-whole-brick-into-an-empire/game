extends Area2D

var defaultColour = Color(1, 0.216, 0)
var mousedColour = Color(1, 0.541, 0)
var offColour = Color(0.533, 0.533, 0.533)

# Called when the node enters the scene tree for the first time.
func _ready():
	$AnimatedSprite2D.play("default")
	
	$AnimatedSprite2D/TextWrapper/Label.text = name
	$AnimatedSprite2D/TextWrapper/Label.set("theme_override_colors/font_color",defaultColour)

signal activated(button_name)

var moused = false
var enabled = true

func _mouse_enter():
	moused = true
	if enabled: 
		$AnimatedSprite2D.play("moused")
		$AnimatedSprite2D/TextWrapper/Label.set("theme_override_colors/font_color",mousedColour)


func _mouse_exit():
	moused = false
	if enabled:
		$AnimatedSprite2D.play("default")
		$AnimatedSprite2D/TextWrapper/Label.set("theme_override_colors/font_color",defaultColour)
	
func _input(event):
	if event is InputEventMouseButton and moused:
		#print_debug(event.button_index)
		if event.button_index == MOUSE_BUTTON_LEFT and (event.pressed == false):
			#print_debug("penis123")
			enabled = false
			activated.emit(name)
			$AnimatedSprite2D.play("off")
			$AnimatedSprite2D/TextWrapper/Label.set("theme_override_colors/font_color",offColour)
