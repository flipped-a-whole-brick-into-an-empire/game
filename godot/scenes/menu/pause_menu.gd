extends Node2D

var next_scene : String

# Called when the node enters the scene tree for the first time.
func _ready():
	Input.mouse_mode = Input.MOUSE_MODE_VISIBLE
	$FadeModuleTrans.play("FadeIn")
	await get_tree().create_timer(1).timeout

func _enter_tree():
	Input.mouse_mode = Input.MOUSE_MODE_VISIBLE
	$FadeModuleTrans.play("FadeIn")
	await get_tree().create_timer(1).timeout

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _on_resume_button_pressed():
	queue_free()
	Input.mouse_mode = Input.MOUSE_MODE_CAPTURED
	get_tree().paused = false

func _on_umrilol_button_pressed():
	$FadeModuleTrans.play("FadeOut")
	get_tree().paused = false
	await get_tree().create_timer(1).timeout
	get_tree().change_scene_to_file("res://scenes/shop/shop.tscn")

func _on_exit_button_pressed():
	$FadeModuleTrans.play("FadeOut")
	get_tree().paused = false
	await get_tree().create_timer(1).timeout
	get_tree().change_scene_to_file("res://scenes/menu/main_menu.tscn")
