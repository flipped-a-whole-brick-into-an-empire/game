extends Node2D


# Called when the node enters the scene tree for the first time.
func _ready():
	$FadeModuleBlack.play("FadeIn")
	await get_tree().create_timer(1).timeout
	
func _enter_tree():
	$FadeModuleBlack.play("FadeIn")
	await get_tree().create_timer(1).timeout
	

var next_scene

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass
	

func _on_play_button_pressed():
	next_scene = "res://scenes/tard_wrangler.tscn"
	$FadeModuleBlack.play("FadeOut")
	await get_tree().create_timer(1).timeout
	get_tree().change_scene_to_file(next_scene)

func _on_quit_button_pressed():
	$FadeModuleBlack.play("FadeOut")
	await get_tree().create_timer(1).timeout
	get_tree().quit()

func _on_credit_button_pressed():
	var credits = preload("res://scenes/menu/credits.tscn").instantiate()
	add_sibling(credits)





