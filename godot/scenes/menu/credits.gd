extends Node2D


# Called when the node enters the scene tree for the first time.
func _enter_tree():
	$AnimationPlayer.play("fly_in")


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _input(event):
	if event is InputEventMouseButton and event.button_index == MOUSE_BUTTON_LEFT:
		$AnimationPlayer.play("fly_out")

func _on_animation_player_animation_finished(anim_name):
	if anim_name == "fly_out":
		queue_free()
