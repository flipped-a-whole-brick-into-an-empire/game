use godot::engine::{CharacterBody3D, ICharacterBody3D};
use godot::engine::input::MouseMode;
use godot::engine::{InputEvent, InputEventMouse};
use godot::prelude::*;
use godot::engine::object::ConnectFlags;

const CAMTILT: f32 = 20.0;

/// Player
#[derive(GodotClass)]
#[class(base=CharacterBody3D)]
pub struct Player {
	#[export]
	health: u8,
	#[export]
	strafespeed: f32,
	#[export]
	runspeed: f32,
	#[export]
	attackrange: f32,
	#[export]
	jumpstrength: f32,
	#[export]
	gravity: f32,
	dodgestate: DodgeState,
	base: Base<CharacterBody3D>,
}

#[derive(Debug, PartialEq)]
enum DodgeState {
	No = 0,
	Left,
	Right,
	Up,
}

#[godot_api]
impl ICharacterBody3D for Player {
	fn init(base: Base<CharacterBody3D>) -> Self {
		Self {
			health: 1,
			strafespeed: 100.0,
			runspeed: 120.0,
			attackrange: 1.0,
			jumpstrength: 30.0,
			gravity: 4.0,
			dodgestate: DodgeState::No,
			base,
		}
	}

	fn ready(&mut self) {

		let mut input = Input::singleton();
		input.set_mouse_mode(MouseMode::CAPTURED);
	}

	fn process(&mut self, delta: f64) {
	}

	fn physics_process(&mut self, delta: f64) {
		// turn off jump dodge if on floor
		if self.base().is_on_floor() && self.dodgestate == DodgeState::Up {
			self.dodgestate = DodgeState::No;
		}

		let input = Input::singleton();

		let mut velocity = Vector3::ZERO;
		if input.is_action_pressed("move_forward".into()) {
			velocity += Vector3::FORWARD;
		}
		if input.is_action_pressed("move_back".into()) {
			velocity += Vector3::BACK;
		}
		if input.is_action_pressed("move_left".into()) {
			velocity += Vector3::LEFT;
		}
		if input.is_action_pressed("move_right".into()) {
			velocity += Vector3::RIGHT;
		}
		if input.is_action_pressed("jump".into()) && self.base().is_on_floor() {
			velocity += Vector3::UP;
			self.dodgestate = DodgeState::Up;
		}

		if input.is_action_pressed("dodge_left".into()) && self.dodgestate == DodgeState::No {
			self.dodgestate = DodgeState::Left;
			let cam_position = self.base().get_node_as::<Camera3D>("Camera").get_position();
			let offset = Vector3::LEFT.rotated(Vector3::UP, self.base().get_rotation().y) * CAMTILT;
			self.base()
				.get_node_as::<Camera3D>("Camera")
				.set_position(offset + cam_position);
		}
		if input.is_action_pressed("dodge_right".into()) && self.dodgestate == DodgeState::No {
			self.dodgestate = DodgeState::Right;
			let cam_position = self.base().get_node_as::<Camera3D>("Camera").get_position();
			let offset =
				Vector3::RIGHT.rotated(Vector3::UP, self.base().get_rotation().y) * CAMTILT;
			self.base()
				.get_node_as::<Camera3D>("Camera")
				.set_position(offset + cam_position);
		}
		if !(input.is_action_pressed("dodge_left".into())
			&& input.is_action_pressed("dodge_right".into()))
		{
			match self.dodgestate {
				DodgeState::Left => {
					let cam_position = self.base().get_node_as::<Camera3D>("Camera").get_position();
					let offset =
						Vector3::RIGHT.rotated(Vector3::UP, self.base().get_rotation().y) * CAMTILT;
					self.base()
						.get_node_as::<Camera3D>("Camera")
						.set_position(offset + cam_position);
				}
				DodgeState::Right => {
					let cam_position = self.base().get_node_as::<Camera3D>("Camera").get_position();
					let offset =
						Vector3::LEFT.rotated(Vector3::UP, self.base().get_rotation().y) * CAMTILT;
					self.base()
						.get_node_as::<Camera3D>("Camera")
						.set_position(offset + cam_position);
				}
				_ => (),
			}
		}

		if self.base().is_on_floor() {
			velocity = velocity.normalized();
			// scale accordingly, this should be the correct projection
			velocity.z *= self.runspeed;
			velocity.y *= self.jumpstrength;
			velocity.x *= self.strafespeed;

			velocity = velocity.rotated(Vector3::UP, self.base().get_rotation().y);

			self.base_mut().set_velocity(velocity);
		} else {
			velocity = self.base().get_velocity();
			velocity += Vector3::DOWN * self.gravity * (delta as f32);
			if input.is_action_pressed("move_forward".into()) {
				velocity += Vector3::FORWARD * (delta as f32);
			}
			if input.is_action_pressed("move_back".into()) {
				velocity += Vector3::BACK * (delta as f32);
			}
			if input.is_action_pressed("move_left".into()) {
				velocity += Vector3::LEFT * (delta as f32);
			}
			if input.is_action_pressed("move_right".into()) {
				velocity += Vector3::RIGHT * (delta as f32);
			}
			self.base_mut().set_velocity(velocity);
		}
		self.base_mut().move_and_slide();
	}
}

