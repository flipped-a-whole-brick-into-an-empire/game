use crate::entities::player::Player;

use godot::engine::{CharacterBody3D, ICharacterBody3D};
use godot::prelude::*;

#[derive(GodotClass)]
#[class(base=CharacterBody3D)]
struct Fist {
	attackrange: f32,
	base: Base<CharacterBody3D>,
}

impl ICharacterBody3D for Fist {
	fn init(base: Base<CharacterBody3D>) {
		Self { base }
	}

	fn process(&mut self) {
		//???
	}
}
