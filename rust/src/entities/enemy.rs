use crate::entities::player::Player;

use godot::engine::{CharacterBody3D, ICharacterBody3D};
use godot::prelude::*;
use rand::{thread_rng, Rng};

/// Enemy
#[derive(GodotClass)]
#[class(base=CharacterBody3D)]
pub struct Enemy {
	/// The AI state
	state: State,
	/// The last random value for State machine.
	/// Will roll when player is in danger radius
	lastrand: u8,
	/// The antigravity strength for separation
	#[export]
	antigrav: f32,
	/// The radius for separation
	#[export]
	sepradius: f32,
	/// The danger radius, where enemy will change state
	#[export]
	danradius: f32,
	/// Max acceleration of enemy
	#[export]
	maxaccel: f32,
	/// Max speed of enemy
	#[export]
	maxspeed: f32,
	/// Attack range of enemy
	#[export]
	attackrange: f32,
	/// Holds reference to player
	/// Needs to be `Some` for Anticipate and Attack states
	#[var]
	player: Option<Gd<Player>>,
	/// Holds reference to other enemies for separation behaviour
	others: Vec<Gd<Self>>,
	base: Base<CharacterBody3D>,
}

/// AI states for enemy
#[derive(Debug, PartialEq)]
enum State {
	/// Separation
	Circle = 0,
	/// Hoping player will run into attack
	Anticipate,
	/// Seek + attack at close distance
	Attack,
}

#[godot_api]
impl ICharacterBody3D for Enemy {
	fn init(base: Base<CharacterBody3D>) -> Self {
		Self {
			state: State::Circle,
			lastrand: 0,
			maxaccel: 200.0,
			maxspeed: 200.0,
			attackrange: 40.0,
			sepradius: 50.0,
			antigrav: 20.0,
			danradius: 100.0,
			player: None,
			// NOTE remember to populate this
			others: vec![],
			base,
		}
	}

	fn process(&mut self, delta: f64) {
		let mut rng = thread_rng();
		if let Some(s) = &self.player {
			let dist: Vector3 = s.bind().base().get_position() - self.base().get_position();
			if dist.length() <= self.danradius && self.state == State::Circle {
				// change state randomly
				self.lastrand = rng.gen_range(1..=2);
			}
			if dist.length() > self.danradius {
				self.lastrand = 0;
			}
		} else {
			self.lastrand = 0;
		}
		self.state = match self.lastrand {
			0 => State::Circle,
			1 => State::Anticipate,
			2 => State::Attack,
			_ => unreachable!(),
		};
		if let Some(s) = &self.player {
			let dist: Vector3 = s.bind().base().get_position() - self.base().get_position();

			match self.state {
				State::Circle => (),
				State::Anticipate => (),
				State::Attack => {
					let position = self.base().get_position();
					if dist.length() <= self.attackrange {
						self.base_mut().emit_signal(
							"attack".into(),
							&[Variant::from(position), Variant::from(rng.gen_range(1..=3))],
						);
					}
				}
			}
		}
	}

	fn physics_process(&mut self, delta: f64) {
		let mut velocity = self.base().get_position();
		match &self.state {
			State::Circle => {
				for other in self.others.iter().map(|x| x.bind()) {
					velocity += self.separate(&other.base().get_position(), delta);
				}
			}
			State::Anticipate => {
				velocity = Vector3::ZERO;
			}
			State::Attack => {
				for other in self.others.iter().map(|x| x.bind()) {
					velocity += self.separate(&other.base().get_position(), delta);
				}
				let target = self.player.as_ref().unwrap().bind().base().get_position();
				velocity += self.seek(&target, delta)
			}
		}
		if velocity.length() > self.maxspeed {
			velocity = velocity.normalized() * self.maxspeed;
		}
		self.base_mut().set_velocity(velocity);
	}
}

/// Enemy combat and AI
#[godot_api]
impl Enemy {
	#[signal]
	fn attack(position: Vector3, attacktype: u8);

	fn separate(&self, other: &Vector3, delta: f64) -> Vector3 {
		// vector from obstacle to self
		let dist: Vector3 = self.base().get_position() - *other;
		let mut velocity = Vector3::ZERO;
		if dist.length() <= self.get_sepradius() {
			// NOTE look for error here
			let strength = self.antigrav * dist.length_squared().min(self.maxaccel);
			// NOTE also here
			velocity = dist.normalized() * strength * (delta as f32);
		}
		if velocity.length() > self.get_maxspeed() {
			velocity = velocity.normalized() * self.get_maxspeed();
		}
		velocity
	}

	fn seek(&self, other: &Vector3, delta: f64) -> Vector3 {
		let mut velocity = self.base().get_velocity();
		let dist: Vector3 = *other - self.base().get_position();
		let accel = dist.normalized() * self.get_maxaccel();
		velocity += dist.normalized() * accel * (delta as f32);

		if velocity.length() > self.get_maxspeed() {
			velocity = velocity.normalized() * self.get_maxspeed();
		}
		velocity
	}
}
