use godot::prelude::*;

pub mod cracktangle;
pub mod entities;
pub mod map;

struct MyExtension;

#[gdextension]
unsafe impl ExtensionLibrary for MyExtension {}
