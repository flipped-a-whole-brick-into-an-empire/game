use godot::prelude::*;

#[derive(GodotClass)]
#[class(base=Node)]
struct TardWrangler {
	base: Base<Node>,
}

impl INode for TardWrangler {
	fn init(base: Base<Node>) -> Self {
		Self { base }
	}

	fn ready(&mut self) {
		// hide tiles
	}

	fn update(&mut self, delta: f64) {}
}

#[godot_api]
impl TardWrangler {
	#[signal]
	fn player_move();
}
