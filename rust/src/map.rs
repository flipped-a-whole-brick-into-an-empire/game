// nepollovat rodice
// godot connection - očekávat connection pointy?

#![allow(unused)]
use godot::engine::{INode3D, IStaticBody3D, Node3D, StaticBody3D};
use godot::prelude::*;
use ordered_hash_map::OrderedHashMap;
use parking_lot::{Mutex, RwLock};
use petgraph::algo::k_shortest_path;
use petgraph::prelude::*;
use rand::rngs::SmallRng as WyRand;
use rand::{Rng, SeedableRng};
use std::fmt::Debug;
use std::sync::{Arc, Weak};
use uuid::Uuid;

use std::collections::HashMap;

#[derive(GodotClass)]
#[class(base=StaticBody3D)]
struct Crossroad {
	base: Base<StaticBody3D>,
}

#[godot_api]
impl IStaticBody3D for Crossroad {
	fn init(base: Base<StaticBody3D>) -> Self {
		Self { base }
	}
}

#[godot_api]
impl Crossroad {
	fn give_pos(&mut self, orient: Orientation) -> (Orientation, Vector3) {
		let mafia = self.base_mut().get_node_as::<Node>("MafiaConnections");
		let mafia = mafia.get_children().get(0);
		let mafia = mafia.cast::<Node3D>();

		let position = mafia.get_position();
		let orientation = match mafia.get_name().to_string().as_str() {
			"North" => Orientation::North,
			"South" => Orientation::South,
			"East" => Orientation::East,
			"West" => Orientation::West,
			_ => unreachable!(),
		};
		(orientation, position)
	}
}

#[derive(GodotClass)]
#[class(base=StaticBody3D)]
struct Tunnel {
	base: Base<StaticBody3D>,
}

impl Tunnel {
	fn give_pos(&mut self, orient: Orientation) -> (Orientation, Vector3) {
		let mafia = self.base_mut().get_node_as::<Node>("MafiaConnections");
		let mafia = mafia.get_children().get(0);
		let mafia = mafia.cast::<Node3D>();

		let position = mafia.get_position();
		let orientation = match mafia.get_name().to_string().as_str() {
			"North" => Orientation::North,
			"South" => Orientation::South,
			_ => unreachable!(),
		};
		(orientation, position)
	}
}

#[godot_api]
impl IStaticBody3D for Tunnel {
	fn init(base: Base<StaticBody3D>) -> Self {
		Self { base }
	}
}

#[derive(GodotClass)]
#[class(base=StaticBody3D)]
struct Platform {
	base: Base<StaticBody3D>,
}

impl Platform {
	fn give_pos(&mut self, orient: Orientation) -> (Orientation, Vector3) {
		let mafia = self.base_mut().get_node_as::<Node>("MafiaConnections");
		let mafia = mafia.get_children().get(0);
		let mafia = mafia.cast::<Node3D>();

		let position = mafia.get_position();
		let orientation = match mafia.get_name().to_string().as_str() {
			"East" | "East2" => Orientation::East,
			"West" | "West2" => Orientation::West,
			_ => unreachable!(),
		};
		(orientation, position)
	}
}

#[godot_api]
impl IStaticBody3D for Platform {
	fn init(base: Base<StaticBody3D>) -> Self {
		Self { base }
	}
}

#[derive(GodotClass)]
#[class(base=StaticBody3D)]
struct Deadend {
	base: Base<StaticBody3D>,
}

impl Deadend {
	fn give_pos(&mut self, orient: Orientation) -> (Orientation, Vector3) {
		let mafia = self.base_mut().get_node_as::<Node>("MafiaConnections");
		let mafia = mafia.get_children().get(0);
		let mafia = mafia.cast::<Node3D>();

		let position = mafia.get_position();
		let orientation = match mafia.get_name().to_string().as_str() {
			"North" => Orientation::North,
			_ => unreachable!(),
		};
		(orientation, position)
	}
}

#[godot_api]
impl IStaticBody3D for Deadend {
	fn init(base: Base<StaticBody3D>) -> Self {
		Self { base }
	}
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum Orientation {
	North,
	East,
	West,
	South,
}

impl Orientation {
	fn inverse(&self) -> Self {
		use Orientation::*;

		match self {
			North => South,
			East => West,
			West => East,
			South => North,
		}
	}
}

#[derive(GodotClass, Debug)]
#[class(base=Node3D)]
pub struct Map {
	//current_node: WeakTile,
	rng: Arc<Mutex<WyRand>>,

	children: Graph<Child, Orientation>,

	#[export]
	crossroad_bias: f32,
	#[export]
	tunnel_bias: f32,
	#[export]
	platform_bias: f32,
	#[export]
	deadend_bias: f32,

	#[export]
	rng_seed: u32,

	base: Base<Node3D>,
}

#[derive(Debug, Clone, Copy)]
enum TileType {
	Crossroad = 0,
	Tunnel,
	Platform,
	Deadend,
}

#[derive(Debug)]
struct Child {
	tiletype: TileType,
}

#[godot_api]
impl Map {
	#[func]
	fn player_moved(&mut self, which_way: GString) {
		// NOTE I know what I am doing
		let root = self.children.externals(Direction::Incoming).next().unwrap();

		let id = which_way
			.to_string()
			.parse::<u32>()
			.expect("maybe problem, maybe not");
		let id = NodeIndex::from(id);

		let mut nodes_to_process = vec![];
		for neighbor in self.children.neighbors(root) {
			if neighbor == id {
				let edge = self.children.find_edge(root, neighbor).unwrap();
				let weight = self.children.edge_weight(edge).unwrap();
				// reverse edge
				//self.children.remove_edge(edge.id());
				//self.children.add_edge(endpoints.1, endpoints.0, *weight);

				nodes_to_process.push((edge, root, neighbor, *weight));
			}
		}

		for (edge_id, endpoint_1, endpoint_0, weight) in nodes_to_process.into_iter() {
			self.children.remove_edge(edge_id);
			self.children.add_edge(endpoint_1, endpoint_0, weight);
		}

		self.update();
	}

	fn update(&mut self) {
		godot_print!("graph state {:?}", &self.children);
		let root: u32 = self
			.children
			.externals(Direction::Incoming)
			.next()
			.unwrap()
			.index() as u32;

		let res = k_shortest_path(
			&self.children,
			NodeIndex::new(root as usize),
			None,
			1,
			|_| 1,
		);
		for (key, val) in res {
			godot_print!("checking {}: {}", key.index(), val);
			if val > 2 {
				self.base_mut()
					.get_node_as::<StaticBody3D>(key.index().to_string())
					.queue_free();
				self.children.remove_node(key);
			}
			godot_print!(
				"maybe edging with {} neighbors",
				self.children.neighbors(key).count()
			);
			if val < 2 && self.children.neighbors(key).count() <= 1 {
				let edge = self
					.children
					.edges(key)
					.next()
					.map(|e| self.children.edge_weight(e.id()).cloned())
					.flatten()
					.unwrap_or(Orientation::North);
				/*let edge = self
				.children
				.edge_weight(edge.id())
				.cloned()*/

				// NOTE might work might not
				let (mut orientation, mut position): (Orientation, Vector3);

				if let Ok(mut me) = self
					.base()
					.get_child(key.index() as i32)
					.unwrap()
					.try_cast::<Deadend>()
				{
					(orientation, position) = me.bind_mut().give_pos(edge);
				}
				if let Ok(mut me) = self
					.base()
					.get_child(key.index() as i32)
					.unwrap()
					.try_cast::<Crossroad>()
				{
					(orientation, position) = me.bind_mut().give_pos(edge);
				}
				if let Ok(mut me) = self
					.base()
					.get_child(key.index() as i32)
					.unwrap()
					.try_cast::<Tunnel>()
				{
					(orientation, position) = me.bind_mut().give_pos(edge);
				}
				if let Ok(mut me) = self
					.base()
					.get_child(key.index() as i32)
					.unwrap()
					.try_cast::<Platform>()
				{
					(orientation, position) = me.bind_mut().give_pos(edge);
				} else {
					(orientation, position) = (Orientation::North, Vector3::default());
				}

				godot_print!("adding tile");
				self.add_tile(key.index() as u32, orientation, position);
			}
		}
		godot_print!("graph state post update {:?}", &self.children);
	}

	fn add_tile(&mut self, node: u32, orientation: Orientation, pos: Vector3) {
		godot_print!("add tile: {} {:?} {}", node, orientation, pos);
		let tiletype = self.children.node_weight(NodeIndex::new(node as usize));
		let rng = rand::random::<f32>();
		let mut newtile = TileType::Tunnel;

		match tiletype.unwrap().tiletype {
			TileType::Crossroad => {
				let scale = 1.0 / (self.tunnel_bias + self.deadend_bias);
				newtile = if rng <= scale * self.tunnel_bias {
					TileType::Tunnel
				} else {
					TileType::Deadend
				};
			}
			TileType::Tunnel => {
				let scale = 1.0 / (self.crossroad_bias + self.tunnel_bias + self.platform_bias);
				newtile = if rng <= scale * self.crossroad_bias {
					TileType::Crossroad
				} else if rng <= scale * self.tunnel_bias {
					TileType::Tunnel
				} else {
					TileType::Platform
				};
			}
			TileType::Platform => {
				newtile = TileType::Tunnel;
			}
			// huh? TileType::Deadend => unreachable!(),
			TileType::Deadend => return,
		}
		// add to graph
		let name = self.children.add_node(Child { tiletype: newtile });
		let edge = self
			.children
			.add_edge(NodeIndex::new(node as usize), name, orientation);
		match newtile {
			TileType::Crossroad => {
				let child = load::<PackedScene>("res://scenes/map_parts.tscn/crossroads.tscn");
				let mut child = child.instantiate_as::<Crossroad>();

				// name child
				child.set_name(name.index().to_string().into());
				// position child
				let mut diff = child.upcast_ref::<StaticBody3D>().get_position();
				let (orientation, connect_position) = child
					.bind_mut()
					.give_pos(*self.children.edge_weight(edge).unwrap());
				diff = connect_position - diff;

				let mut child = child.upcast::<StaticBody3D>();
				child.set_position(pos + diff);
				child.show();

				self.base_mut().add_child(child.upcast());
			}
			TileType::Tunnel => {
				let child = load::<PackedScene>("res://scenes/map_parts.tscn/tunnel.tscn");
				let mut child = child.instantiate_as::<Tunnel>();

				// name child
				child.set_name(name.index().to_string().into());
				// position child
				let mut diff = child.upcast_ref::<StaticBody3D>().get_position();
				let (orientation, connect_position) = child
					.bind_mut()
					.give_pos(*self.children.edge_weight(edge).unwrap());
				diff = connect_position - diff;

				let mut child = child.upcast::<StaticBody3D>();
				child.set_position(pos + diff);
				child.show();

				self.base_mut().add_child(child.upcast());
			}
			TileType::Platform => {
				let child = load::<PackedScene>("res://scenes/map_parts.tscn/platform.tscn");
				let mut child = child.instantiate_as::<Platform>();

				// name child
				child.set_name(name.index().to_string().into());
				// position child
				let mut diff = child.upcast_ref::<StaticBody3D>().get_position();
				let (orientation, connect_position) = child
					.bind_mut()
					.give_pos(*self.children.edge_weight(edge).unwrap());
				diff = connect_position - diff;

				let mut child = child.upcast::<StaticBody3D>();
				child.set_position(pos + diff);
				child.show();

				self.base_mut().add_child(child.upcast());
			}
			TileType::Deadend => {
				let child = load::<PackedScene>("res://scenes/map_parts.tscn/deadend.tscn");
				let mut child = child.instantiate_as::<Deadend>();

				// name child
				child.set_name(name.index().to_string().into());
				// position child
				let mut diff = child.upcast_ref::<StaticBody3D>().get_position();
				let (orientation, connect_position) = child
					.bind_mut()
					.give_pos(*self.children.edge_weight(edge).unwrap());
				diff = connect_position - diff;

				let mut child = child.upcast::<StaticBody3D>();
				child.set_position(pos + diff);
				child.show();

				self.base_mut().add_child(child.upcast());
			}
		}
	}
}

#[godot_api]
impl INode3D for Map {
	fn init(base: Base<Node3D>) -> Self {
		let rng = Arc::new(Mutex::new(WyRand::from_entropy()));

		// make sure the tree is rooted
		let mut graph: Graph<Child, Orientation> = Graph::new();

		Self {
			children: graph,
			//	current_node: Arc::downgrade(&initial_crossroads),
			rng,

			deadend_bias: 1.0,
			tunnel_bias: 1.0,
			crossroad_bias: 1.0,
			platform_bias: 1.0,

			rng_seed: 0,
			base,
		}
	}

	fn ready(&mut self) {
		// set up initial crossroad
		let initial_crossroad = Child {
			tiletype: TileType::Crossroad,
		};

		let name = self.children.add_node(initial_crossroad);
		let mut child = Gd::from_init_fn(|base| Crossroad::init(base));
		child.set_name(name.index().to_string().into());
		child.show();

		godot_print!("{:?}", child);
		self.base_mut().add_child(child.upcast());

		// hide children
		self.base().get_node_as::<Deadend>("Deadend").hide();
		self.base().get_node_as::<Crossroad>("Crossroads").hide();
		self.base().get_node_as::<Tunnel>("Tunnel").hide();
		self.base().get_node_as::<Platform>("Platform").hide();

		// populate
		self.update();
		self.update();
	}
}
