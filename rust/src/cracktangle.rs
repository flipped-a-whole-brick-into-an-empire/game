use godot::engine::{AnimationPlayer, ColorRect, IColorRect};
use godot::prelude::*;

use std::time::{Duration, SystemTime};

#[derive(GodotClass)]
#[class(base=ColorRect)]
struct FadingCracktangle {
	base: Base<ColorRect>,
	start: SystemTime,
	player: Option<Gd<AnimationPlayer>>,
}

#[godot_api]
impl IColorRect for FadingCracktangle {
	fn init(base: Base<ColorRect>) -> Self {
		Self {
			base,
			player: None,
			start: SystemTime::now(),
		}
	}

	fn ready(&mut self) {
		let animation_player = self.base().get_child(0).unwrap();
		if let Ok(player) = animation_player.try_cast::<AnimationPlayer>() {
			self.player = Some(player);
		}

		self.player
			.as_mut()
			.unwrap()
			.set_current_animation("Fade".into());
		self.player.as_mut().unwrap().play_backwards();
	}

	fn process(&mut self, _delta: f64) {
		if let Ok(dur) = SystemTime::now().duration_since(self.start) {
			if dur > Duration::from_secs(2) {
				self.base()
					.get_tree()
					.expect("FUCK YOU")
					.change_scene_to_file("res://scenes/intro/main_menu.tscn".into());
			}
		}
	}
}
